import * as d3 from "d3";
import topo from "../src/world.json";

let data = {};
// retrieving the data

Promise.all([


  d3.csv(
    "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv",
    (row) => {
      let country = row["Country/Region"];
      let stats = [];

      delete row["Lat"];
      delete row["Long"];
      delete row["Country/Region"];
      delete row["Province/State"];
      let result = { name: country, stats: row };

      data[country] = row;
    }
  ),
]).then(() => {
  

  

  function biWGrowthrate(countryName) {
    let e = Object.values(data[countryName]);

    function valueDaysAgo(p) {
      if (e.length < p )  {return 0}
      else {return e[e.length - p - 1]};
    }



    let a = valueDaysAgo(28);
    let b = valueDaysAgo(14);
    let c = valueDaysAgo(0);

    function rate(a, b, c) {
      let rateOne = b - a,
        rateTwo = c - b

        if(rateOne == 0 && rateTwo == 0) {return 0} 
        else if (rateOne != 0 && rateTwo != 0) {
           return ((rateTwo - rateOne) / rateOne) * 100
        }
        else  {return rateOne > rateTwo ? -100 : 100}
        
    
    }

    let growthRate = rate(a, b, c);
    return  Math.round(growthRate * 100) / 100; 
  }


  function calcGrData() {
    let d = new Map();
    Object.keys(data).forEach((c) => {
      let growthRate = biWGrowthrate(c);
      d.set(c, growthRate);
    });
    return d;
  }

  let grData = calcGrData();

  function evaluateTrend(gr) {
    if (gr < 0) {
      return "↘";
    } else if(gr == 0) {  return "→";
    }else {
      return "↗";
    }
  }



  //////////////////
  /// CHOROPLETH ///
  //////////////////

  const projection = d3.geoNaturalEarth1().center([15, 13]).scale(200);

  const path = d3.geoPath().projection(projection);

  let svg = d3
    .select("#covid-update")
    .attr("width", "1000")
    .attr("height", "auto")
    .attr("max-width", "100%")
    .attr("margin", "auto")
    .append("svg")
    .attr("viewBox", "0 0 960 500");

  let g = svg.append("g");



  function showTooltip(e, d) {
    let c = d.properties["ADMIN"]
    if (!(c in data)) {
      tooltip.classed("hidden", true);
    } else {
        let info = `${c}: ${grData.get(c)} ${evaluateTrend(grData.get(c))}`
      d3.select(e.target).attr("opacity", 0.4).style("cursor", "pointer");
      tooltip.classed("hidden", false).html(info);
    }
  }


  var tooltip = d3
    .select("body")
    .append("div")
    .attr("class", "tooltip hidden")
    .attr("style", "position:absolute");


  // color scale

  var keys = [-100, -50, -25, 0, 25, 50, 100];
  var colorScale = d3.scaleThreshold().domain(keys).range(d3.schemePurples[7]);



  g.selectAll("path")
    .data(topo.features)
    .enter()
    .append("path")
    // draw each country
    .attr("d", path) 
    // set the color of each country
    .attr("stroke-width", 0.1)
    .attr("stroke", "white")
    .attr("fill", function (d) {
      d.total = grData.get(d.properties["ADMIN"]) ;
  
      return ( d.total === undefined ? "#D3D3D3" : colorScale(d.total));
    })
    .on("mouseover", function (e, d) {
      showTooltip(e, d);
    })
    .on("mousemove", function (e) {
      const [x, y] = d3.pointer(e, d3.select("body"));

      tooltip.style("top", y + 15 + "px").style("left", x + 20 + "px");
    })
    .on("mouseout", function () {
      d3.select(this).attr("opacity", 1).style("cursor", "default");
      tooltip.classed("hidden", true);
    });


  let legend = d3
    .select("#covid-update")
    .append("div")
    .attr("style", "width:600px;height:auto;max-width:100%;")
    .append("svg")
    .attr("viewBox", "0 0 500 70")
    .selectAll("mydots")
    .data(keys)
    .enter();

  legend
    .append("rect")
    .attr("x", function (d, i) {
      return 10 + i * (25 + 1);
    })
    .attr("y", 10)
    .attr("height", 8)
    .attr("width", 25)
    .attr("text-anchor", "middle")
    .style("fill", function (d) {
      return colorScale(d);
    });

  legend
    .selectAll("mylabels")
    .data(keys)
    .enter()
    .append("text")
    .attr("x", function (d, i) {
      return 10 + i * (25 + 1);
    })
    .attr("y", 30)
    .text(function (d) {
      return d;
    })
    .attr("text-anchor", "middle")
    .style("alignment-baseline", "middle")
    .attr("class", "legend")
  

  legend
    .append("text")
    .attr("x", 250)
    .attr("y", 20)
  
    .text("Biweekly growth rate of COVID-19 cases")
    .attr("text-anchor", "left")
    .style("alignment-baseline", "middle")
    .attr("class", "legend")


  legend
    .append("text")
    .attr("x", 250)
    .attr("y", 35)
    .text("compared to the previous two weeks.")
    .attr("text-anchor", "left")
    .style("alignment-baseline", "middle")
    .attr("class", "legend")
  
});
