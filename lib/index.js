"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (obj) { return typeof obj; } : function (obj) { return obj && "function" == typeof Symbol && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }, _typeof(obj); }
var d3 = _interopRequireWildcard(require("d3"));
var _world = _interopRequireDefault(require("../src/world.json"));
function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }
function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }
function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }
function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function _iterableToArrayLimit(arr, i) { var _i = null == arr ? null : "undefined" != typeof Symbol && arr[Symbol.iterator] || arr["@@iterator"]; if (null != _i) { var _s, _e, _x, _r, _arr = [], _n = !0, _d = !1; try { if (_x = (_i = _i.call(arr)).next, 0 === i) { if (Object(_i) !== _i) return; _n = !1; } else for (; !(_n = (_s = _x.call(_i)).done) && (_arr.push(_s.value), _arr.length !== i); _n = !0); } catch (err) { _d = !0, _e = err; } finally { try { if (!_n && null != _i["return"] && (_r = _i["return"](), Object(_r) !== _r)) return; } finally { if (_d) throw _e; } } return _arr; } }
function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }
var data = {};
// retrieving the data

Promise.all([d3.csv("https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv", function (row) {
  var country = row["Country/Region"];
  var stats = [];
  delete row["Lat"];
  delete row["Long"];
  delete row["Country/Region"];
  delete row["Province/State"];
  var result = {
    name: country,
    stats: row
  };
  data[country] = row;
})]).then(function () {
  function biWGrowthrate(countryName) {
    var e = Object.values(data[countryName]);
    function valueForDay(p) {
      return e[e.length - p];
    }
    var a = valueForDay(30);
    var b = valueForDay(16);
    var c = valueForDay(2);
    function rate(a, b, c) {
      var rateOne = b - a,
        rateTwo = c - b;
      if (rateOne != 0 && rateTwo != 0) {
        return (rateTwo - rateOne) / rateOne * 100;
      } else if (rateOne == 0 && rateTwo == 0) {
        return 0;
      } else if (rateTwo > rateOne) {
        return rateTwo / b * 100;
      } else {
        return rateOne / a * 100;
      }
    }
    ;
    var growthRate = rate(a, b, c);
    return Math.round(growthRate * 100) / 100;
  }
  function calcGrData() {
    var d = new Map();
    Object.keys(data).forEach(function (c) {
      var growthRate = biWGrowthrate(c);
      d.set(c, growthRate);
    });
    return d;
  }
  var grData = calcGrData();
  function evaluateTrend(gr) {
    if (gr < 0) {
      return "↘";
    } else if (gr == 0) {
      return "→";
    } else {
      return "↗";
    }
  }
  console.log(Object.values(data["China"]));
  //////////////////
  /// CHOROPLETH ///
  //////////////////

  var projection = d3.geoNaturalEarth1().center([15, 13]).scale(200);
  var path = d3.geoPath().projection(projection);
  var svg = d3.select("#covid-update").attr("width", "1000").attr("height", "auto").attr("max-width", "100%").attr("margin", "auto").append("svg").attr("viewBox", "0 0 960 500");
  var g = svg.append("g");
  function showTooltip(e, d) {
    var c = d.properties["ADMIN"];
    if (!(c in data)) {
      tooltip.classed("hidden", true);
    } else {
      var info = "".concat(c, ": ").concat(grData.get(c), " ").concat(evaluateTrend(grData.get(c)));
      d3.select(e.target).attr("opacity", 0.4).style("cursor", "pointer");
      tooltip.classed("hidden", false).html(info);
    }
  }
  var tooltip = d3.select("body").append("div").attr("class", "tooltip hidden").attr("style", "position:absolute");

  // color scale

  var keys = [-100, -50, -25, 0, 25, 50, 100];
  var colorScale = d3.scaleThreshold().domain(keys).range(d3.schemePurples[7]);
  g.selectAll("path").data(_world["default"].features).enter().append("path")
  // draw each country
  .attr("d", path)
  // set the color of each country
  .attr("stroke-width", 0.1).attr("stroke", "white").attr("fill", function (d) {
    d.total = grData.get(d.properties["ADMIN"]);
    return d.total === undefined ? "#D3D3D3" : colorScale(d.total);
  }).on("mouseover", function (e, d) {
    showTooltip(e, d);
  }).on("mousemove", function (e) {
    var _d3$pointer = d3.pointer(e, d3.select("body")),
      _d3$pointer2 = _slicedToArray(_d3$pointer, 2),
      x = _d3$pointer2[0],
      y = _d3$pointer2[1];
    tooltip.style("top", y + 15 + "px").style("left", x + 20 + "px");
  }).on("mouseout", function () {
    d3.select(this).attr("opacity", 1).style("cursor", "default");
    tooltip.classed("hidden", true);
  });
  var legend = d3.select("#covid-update").append("div").attr("style", "width:600px;height:auto;max-width:100%;").append("svg").attr("viewBox", "0 0 500 70").selectAll("mydots").data(keys).enter();
  legend.append("rect").attr("x", function (d, i) {
    return 10 + i * (25 + 1);
  }).attr("y", 10).attr("height", 8).attr("width", 25).attr("text-anchor", "middle").style("fill", function (d) {
    return colorScale(d);
  });
  legend.selectAll("mylabels").data(keys).enter().append("text").attr("x", function (d, i) {
    return 10 + i * (25 + 1);
  }).attr("y", 30).text(function (d) {
    return d;
  }).attr("text-anchor", "middle").style("alignment-baseline", "middle").attr("class", "legend");
  legend.append("text").attr("x", 250).attr("y", 20).text("Biweekly growth rate of COVID-19 cases").attr("text-anchor", "left").style("alignment-baseline", "middle").attr("class", "legend");
  legend.append("text").attr("x", 250).attr("y", 35).text("compared to the previous two weeks.").attr("text-anchor", "left").style("alignment-baseline", "middle").attr("class", "legend");
});