# Covid Update Live Map
This is an interactive map shows the biweekly growth rate of new Covid-19 cases. The biweekly growth rate measures the percentage change in the number of new confirmed cases over the last 14 days relative to the number in the previous 14 days.

The script fetches daily updated case data from <a href="https://github.com/CSSEGISandData/COVID-19">John Hopkins University</a>.